Lecture for Methods of Professional Software Engineering
********************************************************

.. toctree::
    :maxdepth: 2
    :glob:
    :caption: Markdown

    markdown/index

.. toctree::
    :maxdepth: 2
    :glob:
    :caption: Terminal

    terminal/index

.. toctree::
    :maxdepth: 2
    :caption: Git

    git/index


.. toctree::
    :maxdepth: 2
    :caption: GitLab

    gitlab/index

.. toctree::
    :maxdepth: 2
    :caption: Python

    python/index

.. toctree::
    :maxdepth: 2
    :caption: PyCharm

    pycharm/index
