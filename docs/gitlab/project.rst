Projects
========

.. contents:: Table of contents
    :local:

Create new project
------------------

#. Create a new project by clicking "New project" in a GitLab subgroup.

   .. figure:: images/new-project.png

#. Define a project name and choose private or public visibility level. Optionally set a project description.

   .. figure:: images/create-project.png

#. After the project is created, you can see an overview.

   .. figure:: images/created-project.png

#. Click the button "Clone" and copy the HTTPS URL.

   .. figure:: images/clone-project.png
