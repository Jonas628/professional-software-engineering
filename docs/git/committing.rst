Committing
==========

.. contents:: Table of contents
    :local:

Golden rules of writing commit messages
---------------------------------------

1.  Separate the subject from the body with a blank line
2.  Your commit message should not contain any whitespace errors
3.  Remove unnecessary punctuation marks
4.  Do not end the subject line with a period
5.  Capitalize the subject line and each paragraph
6.  Use the imperative mood in the subject line
7.  Use the body to explain what changes you have made and why you made
    them.
8.  Do not assume the reviewer understands what the original problem
    was, ensure you add it.
9.  Do not think your code is self-explanatory
10. Follow the commit convention defined by your team

    According to
    https://www.freecodecamp.org/news/writing-good-commit-messages-a-practical-guide/
