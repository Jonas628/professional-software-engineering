Git Projects
============

.. contents:: Table of contents
    :local:

Where should I store my Git projects?
-------------------------------------

A good practice is to create a ``git`` folder in your home directory and store projects in subdirectories depending on the remotes. The table lists the home folders of different operating systems.

+----------------------+------------------------+
| OS                   | Path to home           |
+======================+========================+
| Windows              | ``C:\Users\<USER>\``   |
+----------------------+------------------------+
| Windows (Git Bash)   | ``C/Users/<USER>/``    |
+----------------------+------------------------+
| Linux                | ``/home/<USER>/``      |
+----------------------+------------------------+

Let us introduce the variable ``$HOME`` which is a virtual reference of the path listed above depending on your operating system. In the following, this variable is used to simplify the commands in the code listings. But be careful with the directory separators, on Windows you have to use ``\`` instead of ``/`` if your are not using Git Bash for
Windows.

There are many websites hosting Git platforms. The most famous Git hosting platforms are `GitLab <https://gitlab.com>`__ and
`GitHub <https://github.com>`__. To differentiate between those platforms (in the following named *remote*) you should put your git
projects in subdirectories named by the remote.

+------------+-----------------------------+
| Platform   | Path                        |
+============+=============================+
| GitLab     | ``$HOME/git/gitlab.com/``   |
+------------+-----------------------------+
| GitHub     | ``$HOME/git/github.com/``   |
+------------+-----------------------------+

Depending on the hosting platform, additional subdirectories can be used when creating projects. You should also map those directory structures to your local directory tree.

Following this convention, you should create the folder ``$HOME/git/gitlab.com/hs-karlsruhe/`` as a base directory for this project for example.

Before you start using Git on the command line, setting user and email is required:

.. code:: bash

    $ git config --global user.name "Example Name"
    $ git config --global user.email "mail@example.org"

**Hints:**

-  Every command line command starts with ``$``, the output of the
   command is listed below without leading characters.

Starting from local
-------------------

When you want to start a Git project locally, you should think of where you want to publish (``git push``) it later.

For this project, we would create a folder and initialize the Git project:

.. code:: bash

    $ mkdir -p $HOME/git/gitlab.com/hs-karlsruhe/professional-software-engineering
    $ cd $HOME/git/gitlab.com/hs-karlsruhe/professional-software-engineering
    $ git init
    Initialized empty Git repository in $HOME/git/gitlab.com/hs-karlsruhe/professional-software-engineering/.git/

**Hints:**

-  With ``mkdir -p`` we can create new folders recursively.

In this state, you can work on your project locally and you can use almost any Git functions. But you can't push your commits until you configure a remote with a URL.

So let's create an empty ``README.md`` file, add it to the staging area and commit the changes.

.. code:: bash

    $ touch README.md
    $ git add README.md
    $ git commit -m "Initial commit"

Next, add the remote ``origin`` with

.. code:: bash

    $ git remote add origin https://gitlab.com/hs-karlsruhe/professional-software-engineering.git

**Hints:**

-  The suffix ``.git`` at the end of the URL is optional but it is preferred using it.

Finally, you can push your commits to the remote. At this time, you have to set the remote (only once) with ``--set-upstream origin`` because the branch does not already exist on the remote. So you have to link your local branch with the remote branch.

If you added the remote and you use GitLab, you can push your repository without creating a project in GitLab first. On the first push of a project, GitLab creates a private repository for you. The output of the command tells you, when a project was created.

.. code:: bash

    $ git push --set-upstream origin master
    Enumerating objects: 3, done.
    Counting objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 875 bytes | 875.00 KiB/s, done.
    Total 3 (delta 0), reused 0 (delta 0)
    remote:
    remote:
    remote: The private project hs-karlsruhe/professional-software-engineering was successfully created.
    remote:
    remote: To configure the remote, run:
    remote:   git remote add origin git@gitlab.com:hs-karlsruhe/professional-software-engineering.git
    remote:
    remote: To view the project, visit:
    remote:   https://gitlab.com/hs-karlsruhe/professional-software-engineering
    remote:
    remote:
    To https://gitlab.com/hs-karlsruhe/professional-software-engineering.git
     * [new branch]      master -> master
    Branch 'master' set up to track remote branch 'master' from 'origin'.

Starting from remote
--------------------

When you want to use an already existing project hosted on a Git platform, you need to clone (``git clone``) the project to your local
computer.

For this project, we would create a folder and clone the Git project (*remote*). When cloning a project, Git will always create a new folder with the name of the remote repository.

.. code:: bash

    $ mkdir -p $HOME/git/gitlab.com/hs-karlsruhe
    $ cd $HOME/git/gitlab.com/hs-karlsruhe
    $ git clone https://gitlab.com/hs-karlsruhe/professional-software-engineering.git

**Hints:**

-  The suffix ``.git`` at the end of the URL is optional but it is preferred using it.
-  The folder ``professional-software-engineering`` is automatically created

When you clone a project, the remote of the default branch (usually ``master``) is always set to ``origin`` so you do not have to explicitly use ``git push origin <branch>`` but only ``git push``.
