Webservice
==========

.. contents:: Table of contents
    :local:

Getting started
---------------

We will use `Flask <https://flask.palletsprojects.com/en/1.1.x/>`__ to develop web services. Install ``flask`` as a runtime package into your pipenv project to start building a webserver.

This code snippet provides a very simple Flask web app.

.. code:: python

    from flask import Flask

    app = Flask(__name__)


    @app.route('/')
    def hello_world():
        return 'Hello, World!'


    if __name__ == "__main__":
        app.run(debug=False, host="0.0.0.0", port=8080)

Routing
-------

This section is partially adopted from the official Flask documentation at https://flask.palletsprojects.com/en/1.1.x/quickstart/#routing.

Modern web applications use meaningful URLs to help users. Users are more likely to like a page and come back if the page uses a meaningful URL they can remember and use to directly visit a page.

Use the :meth:`~flask.Flask.route` decorator to bind a function to a URL.

.. code:: python

    @app.route('/')
    def index():
        return 'Index Page'

    @app.route('/hello')
    def hello():
        return 'Hello, World'

You can do more! You can make parts of the URL dynamic and attach multiple rules to a function.

Variable Rules
``````````````

You can add variable sections to a URL by marking sections with ``<variable_name>``. Your function then receives the ``<variable_name>`` as a keyword argument. Optionally, you can use a converter to specify the type of the argument like ``<converter:variable_name>``.

.. code:: python

    from markupsafe import escape

    @app.route('/user/<username>')
    def show_user_profile(username):
        # show the user profile for that user
        return f'User {username}'

    @app.route('/post/<int:post_id>')
    def show_post(post_id):
        # show the post with the given id, the id is an integer
        return f'Post {post_id}'

Converter types:

========== ==========================================
``string`` (default) accepts any text without a slash
``int``    accepts positive integers
``float``  accepts positive floating point values
========== ==========================================

Abort requests with an error code
---------------------------------

A request can always be finished by using function ``abort(status_code, description)``, which finished the response with the given status code and a custom description. After executing the function ``abort()``, the code execution in the route function is aborted.

In the provided code example, the return statement is never executed when the length of the username is less than 3 characters.

.. code:: python

    @app.route('/users/<user>')
    def get_user(user):
        if len(user) < 3:
            abort(400, description="The username must exists of at least 3 characters.")
        return "The username is valid."

Returning JSON
--------------

Flask automatically returns a JSON formatted output, if the return value is a dictionary.

The code snippet

.. code:: python

    @app.route('/users')
    def get_users():
        return {
            '1': {
                'surename': 'Anna',
                'lastname': 'Nass'
            },
            '2': {
                'surename': 'Oliver',
                'lastname': 'Himmel'
            }
        }

returns the following output when calling ``/users``:

.. code::

    {"1":{"lastname":"Nass","surename":"Anna"},"2":{"lastname":"Himmel","surename":"Oliver"}}


Testing
-------

Flask provides a ``test_client()`` function for every defined Flask app. This test client provides a convenient way to test the functionality of the webservice.

Before a test instance of the app can be created, the app must be imported from the module where it is defined. The test instance offers basic methods for calling a URL and test the returned result.

The simplest test is to call the root URL (``test_app.get("/")``). This method performs a HTTP GET call and returns a response object. See some  important attributes of the response in the following table.

.. list-table:: Important attributes of the response object.
    :widths: auto
    :header-rows: 1

    * - Attribute
      - Description

    * - ``data``
      - Body of the response with type ``byte``. Can be converted to type ``string`` by using ``decode()`` function.

    * - ``status_code``
      - Returned HTTP status code. A list of all status codes with code and description can be found at https://developer.mozilla.org/en-US/docs/Web/HTTP/Status.

.. code:: python

    # import your app from the original module

    def test_root():
        with app.test_client() as test_app:
            response = test_app.get("/")
            assert response.status_code == 200
            assert "Hello, World!" in response.data.decode()

Templating
----------

Flask supports the templating engine `Jinja <http://jinja.pocoo.org/docs/templates/>`__ for creating webpages with dynamic contents.

Detailed information can be found at https://flask.palletsprojects.com/en/1.1.x/templating/.

Flask expects Jinja template files in the folder ``templates`` relative to the module path. Usually, HTML template files are ending with the suffix ``.html.j2`` so that developer tools can detect the usage of HTML in combination with Jinja.

An example project structure would look like

.. code-block::

    ├── <project_name>/
    │ ├── templates/
    │ │ └── index.html.j2
    │ ├── __init__.py
    │ └── webserver.py

with a default route serving a small HTML index page in ``webserver.py``:

.. code:: python

    from flask import Flask, render_template

    app = Flask(__name__)

    @app.route('/')
    def index():
        """Returns index page."""
        return render_template('index.html.j2', title="Index", show_author=True, items=["item1", "item2"])

The ```render_template(template_name_or_list, **context)`` <https://flask.palletsprojects.com/en/1.1.x/api/#flask.render_template>`__ function reads the template file ``index.html.j2`` and uses the variables from the context (``title="Index", show_author=True, items=["item1", "item2"]``) to render the output.

When the template file ``index.html.j2`` looks like

.. code:: html

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        {% if show_author %}<meta name="author" content="Stephan Müller"/>{% endif %}
        <title>{{ title }}</title>
    </head>

    <body>
      <ul>
      {% for item in items %}
        <li>{{ item }}</li>
      {% endfor %}
      </ul>
    </body>
    </html>

the output

.. code:: html

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta name="author" content="Stephan Müller"/>
        <title>Title</title>
    </head>

    <body>
      <ul>
        <li>item1</li>
        <li>item2</li>
      </ul>
    </body>
    </html>

will be generated.
