.. _Python documentation:

Documentation
=============

.. contents:: Table of contents
    :local:

Docstrings
----------

    A docstring is a string literal that occurs as the first statement in a module, function, class, or method definition.

    -- \ https://www.python.org/dev/peps/pep-0257/#id15\

Python describes the usage of docstrings in `PEP-0257 <https://www.python.org/dev/peps/pep-0257/>`__ (Python Enhancement Proposals).

Docstrings are encapsulated with triple quotes ``"""``.

Module definition
~~~~~~~~~~~~~~~~~

.. code:: python

    """A very minimalistic web server."""

    def webserver():
        serve()

Function definition
~~~~~~~~~~~~~~~~~~~

without parameters

.. code:: python

    def webserver():
        """Serves a hello world page."""
        serve()

with parameters

.. code:: python

    def sum(operand1, operand2):
        """Calculate sum of two operands.

        :param operand1: First operand
        :param operand2: Second operand
        :return: Sum of two operands
        """
        return operand1 + operand2

Class definition
~~~~~~~~~~~~~~~~

.. code:: python

    class Math:
        """Collection of math operations."""

        def sum(self):
            pass
