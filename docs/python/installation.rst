Installation
============

Download the latest Python version from https://www.python.org/downloads/ and install it.

.. note::
    The following instructions are for Python version 3.9 but it should still be valid for future versions.

#. Activate checkbox "Add Python 3.9 to PATH" so that Python is accessible over the command line. Choose customized installation.

   .. figure:: images/setup-1.png

#. Activate all checkboxes:

   .. figure:: images/setup-2.png

#. Change the install location if desired and click "Install".

   .. figure:: images/setup-3.png

#. You can check for a successful installation by running the command

   .. code:: bash

        python --version

   in a terminal (e.g. Git Bash). The version of Python should be printed.
