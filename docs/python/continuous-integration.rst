Continuous Integration (CI)
===========================

.. contents:: Table of contents
    :local:

Requirements
------------

The GitLab project https://gitlab.com/hs-karlsruhe/ci-templates defines GitLab CI templates which can be used for Python projects. The usage is very easy, because everything is predefined, but the projects must fulfill some requirements:

#. The project structure is created according to :ref:`Python project structure`
#. The name of the project is identical to the Python main package name.

   .. note:: It is allowed and recommended to use dashes in the project names instead of underscores (which are required by Python). The CI template can handle both variants.

      For example, if the GitLab project https://gitlab.com/hs-karlsruhe/professional-software-engineering contained a Python package, it must be named ``professional_software_engineering``.

#. GitLab projects must be created as public projects so that the pipeline budget is not limited and the GitLab pages are automatically published externally.

   .. note:: The project visibility can be changed in the menu under "Settings" -> "General" -> "Visibility, project features, permissions".

Preparing a project
-------------------

Assuming a Python project as described above already exists.

Read the manual https://gitlab.com/hs-karlsruhe/ci-templates/-/blob/master/README.md which describes the usage of the CI templates. A ``.gitlab-ci.yml`` file must be created as documented.

If the pipeline is configured correctly, the GitLab pipeline page (Menu "CI / CD") shows a new pipeline with a reference to the related commit and branch.

    .. figure:: images/ci-pipeline.png
        :alt: ci pipeline

Badges
~~~~~~

If the pipeline runs successfully, some badges (an icon with a title and value) are created by default.

.. figure:: images/ci-badges.png
    :alt: ci badges
    :width: 500

The badges can be included in the README as link with a picture, usually above or below the title of the README. The description in https://gitlab.com/hs-karlsruhe/ci-templates/-/blob/master/README.md provides templated markdown snippets which can be adopted to any other project by replacing the placeholders.

For the example project "https://gitlab.com/hs-karlsruhe.de/ws2020/must0004/my-python-project", the following replacements must be done:

- ``PROJECT_PATH``: "hs-karlsruhe.de/ws2020/must0004/my-python-project"
- ``GITLAB_PAGES_GROUP``: "hs-karlsruhe"
- ``PROJECT_PATH_WITHOUT_GITLAB_PAGES_GROUP``: "ws2020/must0004/my-python-project"

If the badges are included correctly, the image is shown as well as the link targets to a valid location.

.. note:: After the first pipeline finished, it can take up to 30 minutes until the badges are published.

.. note:: The badge "Code coverage" shows ``unknown`` as long as there are no defined Python tests.
