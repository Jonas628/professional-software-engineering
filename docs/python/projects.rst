Python projects
===============

.. contents:: Table of contents
    :local:

.. _Python project structure:

Python project structure
------------------------

.. code::

    ├── .git/
    ├── .venv/
    ├── <project_name>/
    │ ├── __init__.py
    │ ..  *.py
    │ └── <project_name>.py
    ├── docs/
    ├── tests/
    │ ├── __init__.py
    │ ..  test_*.py
    │ └── test_<project_name>.py
    ├── .gitignore
    ├── .gitlab-ci.yml
    ├── Pipfile
    ├── Pipfile.lock
    └── README.md

Git
~~~

A good ``.gitignore`` template for Python can be found at https://www.gitignore.io/api/python. Additionally, you should add the entries ``.venv/`` to exclude the virtual environment and ``.idea/`` to exclude Jetbrains specific project settings.

Pipenv
~~~~~~

As you already know, pipenv creates the two files ``Pipfile`` and ``Pipfile.lock`` as well as the virtual environment in ``.venv/``.

Packages and modules
~~~~~~~~~~~~~~~~~~~~

All relevant information about packages and modules can be found at https://docs.python.org/3/tutorial/modules.html. But not all information is relevant for this lecture. Here is a list of the sections you should really understand (if subitems are not included in the list explicitly, they are not relevant but only the text until the following subitem):

-  `6. Modules <https://docs.python.org/3/tutorial/modules.html#modules>`__
-  `6.1 More on Modules <https://docs.python.org/3/tutorial/modules.html#more-on-modules>`__
-  `6.1.1. Executing modules as scripts <https://docs.python.org/3/tutorial/modules.html#executing-modules-as-scripts>`__
-  `6.4 Packages <https://docs.python.org/3/tutorial/modules.html#packages>`__

All files needed at runtime are located in the package folder ``<project_name>/``. Python does not allow folder or file names with
hyphens (``-``), so you have to replace all hyphens with underscores (``_``).

Every file in the package folder (``*.py``) should have a corresponding test file (``test_*.py``) in the ``tests/`` folder.

Documentation
~~~~~~~~~~~~~

Documentation about the source code is done directly in the related source files. There are Python packages, that generate documentation sites out of the source code documentation. Usually, the files required for automatically generate the sites are stored in the ``docs/`` folder. You can also add additional information (such as a user manual) which are also placed in that folder.

The documentation about the source code as also known as "docstrings".

See at :ref:`Python documentation` to get more information about Python documentation.

Continuous Integration
~~~~~~~~~~~~~~~~~~~~~~

The ``.gitlab-ci.yml`` file defines jobs for GitLab CI such as executing linting checks or test cases. The project https://gitlab.com/hs-karlsruhe/ci-templates provides generic templates which can be referenced in your own CI file. Have a look at the readme of this project to see what content you have to write in the CI file.

Executing Python files
----------------------

Python files can be executed by calling ``python`` with the name of the script as parameter.

.. code-block:: bash

    $ python test.py

