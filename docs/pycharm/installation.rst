Installation and Setup
======================

.. contents:: Table of contents
    :local:

PyCharm is a feature rich editor for Python programming. Currently, there is a commercial professional version and a free community version which is limited in features. Fortunately, students from universities can apply for an educational license at https://www.jetbrains.com/shop/eform/students which allows installing the professional version.

Installation
------------

Visit the download page https://www.jetbrains.com/pycharm/download/ and download the professional version for your operating system.

**Windows**

Download and execute the installer.

**Linux**

The simplest way to install PyCharm on Linux is with using snap package manager:

.. code:: bash

    sudo snap install pycharm-professional --classic

Preferred settings
------------------

Once you installed and started PyCharm the ``Welcome to PyCharm`` window will be opened.

.. figure:: images/welcome-to-pycharm.jpg
   :alt: welcome

Before you begin using PyCharm, you should adjust a few settings.

Click ``Configure -> Settings`` at the welcome page to open the project settings. Later on, if a project is already opened, you can also click on ``File -> Settings``.

``Editor -> General``
~~~~~~~~~~~~~~~~~~~~~

Using soft wraps means wrapping lines at the end of the editor. This allows editing long lines without the need of horizontal scrolling. You can define custom patterns for matching different files, but in the simplest form, just put ``*`` and every text file will be wrapped.

.. figure:: images/pycharm-soft-wrap-lines.jpg
   :alt: pycharm-soft-wrap-lines

It is common practice to end a file with a new empty line. In Python, it is convention to always follow this practice. Also, it is a good practice to remove trailing spaces on lines to remove unnecessary characters and check in clean files to version control. You can enforce both conventions in the section ``Save Files``.

.. figure:: images/pycharm-safe-files.jpg
   :alt: pycharm-safe-files

``Plugins``
~~~~~~~~~~~

You should install the following plugins:

-  Markdown
-  Pylint

``Tools -> Python integrated Tools``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Python community provides different testing frameworks which are also supported by PyCharm. In this lecture, we will use the ``pytest`` testing engine, so this option must be set in section ``Testing``.

.. figure:: images/pycharm-testing.jpg
   :alt: pycharm-testing
