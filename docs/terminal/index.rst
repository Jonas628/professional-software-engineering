Terminal
========

.. contents:: Table of contents
    :local:

Working with nano editor
------------------------

.. list-table::
    :widths: auto
    :align: left
    :header-rows: 1

    * - Shortcut
      - Action

    * - ``Ctrl + o``
      - Save file

    * - ``Ctrl + x``
      - Exit (when content was not changed)

    * - ``Ctrl + x, y, Enter``
      - Exit with saving file (when content was changed)

    * - ``Ctrl + x, n``
      - Exit without saving file (when content was changed)
