#
# Copyright 2021 Stephan Müller
#
# Licensed under the MIT license

import os
import sys

sys.path.insert(0, os.path.abspath('..'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.intersphinx',
]

templates_path = ['_templates']
exclude_patterns = ['_build']
html_static_path = ['_static']

source_suffix = '.rst'
master_doc = 'index'

html_theme = 'sphinx_rtd_theme'
pygments_style = 'sphinx'
htmlhelp_basename = 'professional-software-engineering'

project = 'Professional Software Engineering'

html_context = {
    'css_files': [
        '_static/theme_overrides.css',
    ],
}
