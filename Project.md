# Project

## Task

-   Development of an API with Python that allows recording and getting a list of sensor values
    -   Create a GitLab project in your personal namespace (`https://gitlab.com/hs-karlsruhe/ws2020/<HsKAUser>/`)
    -   Initialize project with pipenv and commit to master
    -   Create project structure with empty files and commit to master
    -   Enable Continous Integration (CI) templates according to <https://gitlab.com/hs-karlsruhe/ci-templates> and commit to master
    -   From now, use **Simple Git Flow with GitLab merge requests** and do not commit directly into master
    -   Create a Python webservice with flask and implement an API

        -   `GET /sensor/temperature/{value}`

            Record a new temperature measurement with `{value}` of type float and timestamp of the request. If `{value}` is not of type float, return HTTP response code `400` with message `Only float values are accepted.` and do not add the value to the list of sensors.

        -   `GET /sensor/temperature`

            Returns list of recorded temperature measurements with timestamp and value as list sorted by timestamp ascending, following the spec below

            ```json
            [
              {
                "time": "2020-12-15 21:20:03.426077",
                "value": 12.9
              },
              {
                "time": "2020-12-15 21:25:03.426597",
                "value": 12.4
              },
              {
                "time": "2020-12-15 21:30:03.425687",
                "value": 11.7
              }
            ]
            ```

## Submitting the final project

Submission deadline is 02.03.2021 at 23:59.

You have to export the project in GitLab under `Settings` -> `General` -> `Advanced` -> `Export project`. You will get an email with a download link. Download the exported project and send it to stephan.mueller@hs-karlsruhe.de.

## Basis of Evaluation

**Tasks**

-   All tasks are completely fulfilled

**Documentation**

-   Developer documentation using markdown syntax with most important information, at least
    -   Project title
    -   Project description
    -   Definition of developer flow (how to contribute)
    -   Prerequisites
    -   How to setup project
    -   How to run project
    -   How to use the API

**Clean Code**

-   Complience with clean code practices

**Git**

-   Complience with [golden rules of writing commit messages](Git-cheatsheet.md#golden-rules-of-writing-commit-messages)
-   Complience with [Simple Git Flow / GitHub Flow](https://guides.github.com/introduction/flow/)
-   Regularly committing changes (> 10 commits in master in total)

**Python**

-   Test coverage
-   Meaningful docstrings

**Linting**

-   Linting (based on <https://gitlab.com/hs-karlsruhe/ci-templates>)
    -   Python
    -   Markdown
    -   Yaml
